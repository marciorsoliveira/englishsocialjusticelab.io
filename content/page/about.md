---
title: Sobre o ESJP
subtitle: Why you won't to join us
comments: false
---

![Teem ESJP](/page/team.jpg)



---

Já pensou em praticar a língua inglesa de um modo totalmente diferente?
Através da dança, política, música, passando por temas como: racismo, gênero, sexualidade, espiritualidade, questões ambientais... venha fazer parte e desconstruir todos os seus traumas de aprendizagem.
[Turmas abertas!](https://forms.gle/gep6fFkt1LEbi2Kx8)

---


### Product/service

- Aulas de inglês de modo acessível e revolucionário
- Turmas: básico - avançado








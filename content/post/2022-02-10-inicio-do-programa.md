---
title: Início do Programa
subtitle: Informações básicas
date: 2022-02-10
tags: ["programa", "didatica", "método"]
---


## Objetivos
O objetivo do Programa ESJ é tornar acessível o estudo da língua inglesa, fomentando espaço que favoreça a superação de eventuais traumas de aprendizagem e subverta o uso elitista do idioma, trabalhando dinâmicas e temáticas que façam sentido a quem aprende, como também reafirmando a capacidade de todes no aprender e utilizar o inglês com fluência. 

Trabalharemos textos literários, acadêmicos e de jornalismo comunitário, além de músicas e poesias de autorias negras e periféricas africanas e da diáspora como suporte para discussões e debates. Temáticas abordadas podem incluir: feminismo negro, questões de gênero/sexualidade, questões ambientais, capitalismo racial, cooperação diaspórica, espiritualidades e economias comunitárias - entre outros temas relevantes para nossa arte, pesquisa, ativismo e solidariedade transnacional. 

## Etapas de aprendizagem
1. **Curso Básico I**:  Este ciclo é para quem sente que precisa iniciar "do zero", porque nunca estudou inglês ou iniciou e parou várias vezes antes de aprender estruturas básicas (por exemplo: pronomes, simple tense, continuous tense). A metodologia é de fácil aprendizagem e não seguimos os parâmetros das instituições tradicionais, fazendo com que alunes se comuniquem em inglês já na primeira aula. Buscamos democratizar a língua em um processo contínuo de descolonização. Trabalhamos a sensibilidade, afetividade e cooperação.
2. **Curso Básico II**: Este ciclo é para quem já aprendeu "um pouco de inglês", e se sente preparadx para começar a se comunicar utilizando-se de estruturas básicas da língua inglesa, entende um pouco (ex. já aprendeu simple tense, continuous tense,  já viu perfect tense, pronomes) mesmo que ainda precise consolidar essas estruturas de vocabulário básico. As aulas serão MAJORITARIAMENTE EM INGLÊS, com discussões sobre os textos e gramática.
5. **Curso de Conversation (Intermediário- Avançado)**: Este ciclo é um espaço alegre e de apoio para se praticar a fala e a escuta. Traga todo o seu ser para a aula! Sua pele, seu cabelo, seu corpo, seu gênero, sua história, sua política e seu contexto estão centrados no processo de aprendizagem de línguas para que você pratique e possa expressar suas realidades, seus conhecimentos, suas ideias e suas verdades, fluentemente em Inglês. Nossa sala de aula não é hierárquica. Colegas ajudam uns aos outros enquanto discutimos textos e vídeos relativos a questões de Justiça Social. Às vezes cantamos, às vezes choramos, mas sempre sorrimos! EXCLUSIVAMENTE EM INGLÊS.



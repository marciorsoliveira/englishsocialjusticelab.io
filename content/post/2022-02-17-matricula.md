---
title: Matricule-se já
subtitle: Confira as opções
date: 2022-02-17
tags: ["matrícula", "financeiro"]
---

Por favor, [preencha a ficha de inscrições](https://forms.gle/gep6fFkt1LEbi2Kx8) e realize o pagamento.

# Matrícula

**BÁSICO 1** - segunda-feira (18h-19h30) OU quinta-feira (18h-19h30) (1 x 1h30 min/semana - Teacher Jackson Schmiedek)
[Clique aqui para pagar](http://pag.ae/7X_wz1DEm)

**BÁSICO 2** - segunda-feira (19h30-21h) OU quinta-feira (19h30-21h) (1 x 1h30 min/semana - Teacher Jackson Schmiedek)
[Clique aqui para pagar](http://pag.ae/7X_wAfTxH)

**BÁSICO 2** -  terça-feira - (18h-19h30) (1 x 1h30 min/semana - Teacher Sadakne Baroudi)
[Clique aqui para pagar](http://pag.ae/7X_x8KsS9)

**CONVERSAÇÃO**: quinta-feira (18h-19h30) OU quinta-feira (19h30-21h) (1 x 1h30 min/semana - Teacher Sadakne Baroudi)
[Clique aqui para pagar](http://pag.ae/7X_x9Tnr2)

**BÁSICO 1** -  terça-feira - (19h30-21h) (1 x 1h30 min/semana - Teacher Jordan Fields)
[Clique aqui para pagar](http://pag.ae/7X_wBKx9m)

**BÁSICO 2** - quarta-feira - (18h-19h30) OU quinta-feira 19h30 (1 x 1h30 min/semana - Teacher Jordan Fields)
[Clique aqui para pagar](http://pag.ae/7X_wD8nM9)


---
Para pagamentos do curso integral (4 parcelas) à vista

-  R$ 380,00 (matrícula/novos alunos) 

Chave pix - englishsocialjustice@gmail.com

- R$ 360,00 (rematrícula/antigos alunos)

Chave pix - englishsocialjustice@gmail.com

---
# `Rematrículas`
## Mudança de nível
Alunas/os veteranas/os poderão seguir para o nível seguinte ou permanecer na  mesma fase do curso  conforme a necessidade. Caso tenha alguma dúvida,  fale com o/a seu/sua   professor/a!

### Rematrículas para antigas(os) alunas(os)

**Plano Básico 1**:  [Clique aqui para pagar](http://pag.ae/7X_C9P54t)

**Plano Básico 2**:  [Clique aqui para pagar](http://pag.ae/7X_CdMJnH)

**Conversação/Avançado**: [Clique aqui para pagar](http://pag.ae/7X_CePJX8)

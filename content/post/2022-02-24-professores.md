---
title: Professores
subtitle: Teaching English for Social Justice
date: 2022-02-24
tags: ["example", "photoswipe"]
--- 

{{< gallery caption-effect="fade" >}}
  {{< figure thumb="-thumb" link="/img/sad.jpg" caption="Sadakne" alt="Teacher Sadakne: Ativiste e pesquisadore independente negre norte-americane" >}}
  {{< figure thumb="-thumb" link="/img/jackson.jpg" caption="Jackson" alt="Teacher Jackson: Estudou Letras: inglês/ português/ literaturas pela Universidade Federal Rural do Rio de Janeiro (UFRRJ)" >}}
  {{< figure thumb="-thumb" link="/img/jordan.jpg" caption="Jordan" alt="Teacher Jordan: Bacharel em História na New Jersey City University, rapper afro-estadunidense" >}}
{{< /gallery >}}
{{< load-photoswipe >}}

<!--more-->

<div style="margin-left: auto;
            margin-right: auto;
            width: 80%">

| Teacher Sadakne | Teacher Jackson  | Teacher Jordan |
| :---: | :---: | :---: |
| Ativiste e pesquisadore independente negre norte-americane, com foco em questões de gênero, raça, geografias negras, questões urbanas. Reside no Brasil desde 2003; é curadore do site [Afro-Rio Walking Tour](https://afroriowalkingtour.com) | Estudou Letras: inglês/ português/ literaturas pela Universidade Federal Rural do Rio de Janeiro (UFRRJ). Tem experiência de mais dez anos como professore de língua inglesa. É professore de língua inglesa na Universidade Internacional das Periferias (UNIperiferias/ IMJA) e tradutore da Revista Periferias. Também é revisore e tradutore textual | Bacharel em História na New Jersey City University, rapper afro-estadunidense. Tem um canal no youtube chamado [UmSoh](https://www.youtube.com/user/NappyBishop/featured). Lecionou em várias escolas nos Estados Unidos, professor e cocriador do curso de Inglês Na Quebrada, ensinado a partir de Raps. Também é certificado em History TEFL

</div>


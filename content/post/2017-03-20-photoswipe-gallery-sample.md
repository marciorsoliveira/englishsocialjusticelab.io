---
title: ETAPAS DE APRENDIZAGEM
subtitle: Programa didático
date: 2017-03-20
tags: ["programa", "didatica"]
---

Como pode ser verificado em  [Formulário de inscrição](https://forms.gle/gep6fFkt1LEbi2Kx8) o programa didático é direto ao ponto.

1. **Curso Básico I**:  Este ciclo é para quem sente que precisa iniciar "do zero", porque nunca estudou inglês ou iniciou e parou várias vezes antes de aprender estruturas básicas (por exemplo: pronomes, simple tense, continuous tense). A metodologia é de fácil aprendizagem e não seguimos os parâmetros das instituições tradicionais, fazendo com que se comunique em inglês já na primeira aula. Buscamos democratizar a língua em um processo contínuo de descolonização. Trabalhamos a sensibilidade, afetividade e cooperação.

2. **Curso Básico II**: Este ciclo é para quem ja aprendeu "um pouco de inglês", e se sente preparadx para começar a se comunicar com estruturas básicas, entende um pouco (ex. já aprendeu simple tense, continuous tense,  já viu perfect tense, pronomes) mesmo que ainda precise consolidar essas estruturas de vocabulário básico. Aulas serão MAJORITARIAMENTE EM INGLÊS, tem discussão de textos, e trabalham bastante gramática.

3. **Curso de Conversation** (Intermediário- Avançado): Este ciclo é um espaço alegre e de apoio para praticar a fala e a escuta. Traga todo o seu ser para a aula! Sua pele, seu cabelo, seu corpo, seu gênero, sua história, sua política e seu contexto estão centrados no processo de aprendizagem de línguas para que você pratique expressar suas realidades, seus conhecimentos, suas ideias e suas verdades, fluentemente em Inglês. Nossa sala de aula não é hierárquica. Colegas ajudam uns aos outros enquanto discutimos textos e vídeos relativos a questões de Justiça Social. Às vezes cantamos, às vezes choramos, mas sempre rimos! EXCLUSIVAMENTE EM INGLÊS

